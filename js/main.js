const form = document.querySelector('.password-form');
const inputs = document.querySelectorAll('input');

const err = document.createElement('div');
err.textContent = 'Нужно ввести одинаковые значения';
err.style.color = 'red';

form.addEventListener('click', (evt) => {
    evt.preventDefault();
    err.remove();
    if(evt.target.tagName === 'I') {
        evt.target.classList.toggle('fa-eye-slash');
        if(evt.target.classList.contains('fa-eye-slash')) {
            evt.target.previousElementSibling.removeAttribute('type');
        } else {
            evt.target.previousElementSibling.setAttribute('type', 'password');
        }
    }
    if(evt.target.tagName === 'BUTTON') {
        if(inputs[0].value === inputs[1].value && inputs[0].value.trim() && inputs[1].value.trim()) {
            alert('You are welcome');
        } else {
            let label = inputs[1].closest('label');
            label.append(err);
        }
    }
});
